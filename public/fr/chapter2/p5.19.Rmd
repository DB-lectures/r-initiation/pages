---
title: "Faire le ménage"
weight: 19
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---


- Pour tester si un objet existe, on utilise la fonction `exists`

- Exécuter:

```{r,eval=F}
x=1
exists("x")
aaaa="x"
exists("a")
exists(aaaa)
exists(wojewoije)
```


- Pour lister les objets créés, on peut utiliser la fonction `ls`

- Exécuter:

```{r,eval=F}
ls()
```

- Pour supprimer un objet de l'environnement global, on utilise la fonction `rm`

```{r,eval=F}
x=1
rm(x)
exists("x")
y=1;x=1
rm(list=c("x","y"))
```

### Exercice

- Supprimer tous les objets de l'environnement global.

---

<details>
  <summary>Solution</summary>
  
```{r,eval=F}
rm(list=ls())
```

</details>

---



