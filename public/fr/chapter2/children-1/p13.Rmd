---
title: "Vecteurs de booléens"
weight: 13
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---

### Création

```{r}
x=c(T,F,T)
(1:10)>5
(1:10)%%2==1
rep(T,3)
grepl(pattern = "r",month.name)
```

A quoi servent les vecteurs de booléens ?

Principalement à filtrer ou indexer et compter.


### Fonctions et opérateurs spécifiques applicables à des booléens

```{r}
x=c(T,F,T)
y=c(T,T,T)
all(x,y)
x|y
x||y
x&y
x&&y
 ?`&&`
is.element(1:3,1:2)
!y
```


### Booléen ou numérique ?

```{r}
T==1
F==0
F*1
F+0
T*1
max(T,0)
T+5
```

```{r}
10*((1:10)<5)+5*((1:10)>=5)
```

### Exercice

Executer 
```{r}
set.seed(1)
x=rpois(1000,1)
```

Compter le nombre de fois ou `x` contient un nombre impair

```{r}
set.seed(1)
x=rpois(1000,1)
```




---

<details>
  <summary>Solution</summary>
  
```{r,eval=T}
(x%%2==1)|>sum()
```

</details>

---