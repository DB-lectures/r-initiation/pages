---
title: "Indexation"
weight: 13.5
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---

L'indexation permet d'accéder aux éléments d'un vecteur selon leur ordre, ou leur nom

### Indexation par l'ordre.
```{r}
x=c(3,7,3,4,5)
x[1]
x[1:2]
x[-1]
```


- Exercice

   - Afficher les noms du 4e et du 8e mois de l'année

---

<details>
  <summary>Solution</summary>
  
```{r,eval=T}
month.name[c(4,8)]
month.name[rep(c(4,8),3)]
```
</details>

---


### Indexation par un vecteur de booléens

```{r}
x=c(3,7,3,4,5)
x[c(F,T,F,F,T)]
x[x%%2==1]
```

- Exercice

    - Exécuter 

```{r}
set.seed(1)
x=runif(1000)
y=runif(1000)
xplusgrand<-(x>y)
```
    - Calculer les quartiles des valeurs de x lorsque x>y

---

<details>
  <summary>Solution</summary>
  
```{r,eval=T}
quantile(x[xplusgrand],probs=c(0,.25,.5,.75,1))
```
</details>

---
    
### Indexation par le nomun vecteur de booléens

On peut attribuer un nom à chaque élément d'un vecteur, et utiliser ces noms comme index:

```{r}
x<-month.name
names(x)<-letters[1:12]
names(x)
x[c("e","a","e")]
```



### jongler entre les index.

```{r}
x<-month.name
names(x)<-letters[1:12]
selection.i=c(1,4)
selection.b=is.element(1:12,selection.i)
selection.i2=which(selection.b)
selection.n=names(x)[selection.b]
selection.i3=which(is.element(names(x),selection.n))

x[selection.i]
x[selection.b]
x[selection.i2]
x[selection.n]
x[selection.i3]

```

