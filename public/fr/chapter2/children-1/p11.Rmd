---
title: "Vecteurs numériques"
weight: 11
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---

### Création rapide

Le vecteur de tous les entiers entre l'entier a et l'entier b est créé par l'opérateur `:`.


```{r,eval=T}
(-3):5
```

La fonction `seq` permet de définir un vecteur de réels espacés régulièrement:

```{r,eval=T}
seq(10,20,length.out=11)
seq(10,20,by=2.1)
```
La fonction `rep` permet de répéter un vecteur plusieurs fois:

```{r,eval=T}
rep(1:3,2)
rep(1:3,each=2)
```


### Application des fonctions
- L'application des fonctions à des vecteurs retourne en général un vecteur de la même dimension: la fonction est appliquée à chaque élément du vecteur.

```{r,eval=T}
exp(0:3)
sqrt(1:3)
floor(seq(1,5,by=.3))
ceiling(seq(1,5,by=.3))
signif(30.341,3)
round(30.341)
round(30.341,2)
```

- Certaines fonctions retournent une seule valeur:

```{r,eval=T}
sum(1:3)
min(1:3)
prod(1:3)
```
- Les fonctions pouvant être appliquées à un nombre arbitraire de réels peuvent aussi en général être appliquées à un nombre arbitraire de vecteurs de réels:


```{r,eval=T}
sum(1:3,2:9,3)
min(1:3,2:10,0)
prod(1:3,2:5)
```

- pour obtenir un extremum terme à terme, il faut utiliser une des fonctions `pmin` ou `pmax`:

```{r,eval=T}
pmin(1:3,2:4,c(0,8,3))
```

les opérateurs binaires d'arithmétique s'appliquent aussi aux vecteurs de même longueur:

```{r,eval=T}
(1:3)+(2:4)*c(3,2,1)-c(9,4,5)^c(2,2,0)
x=(1:3)
y=(2:4)
z=c(3,2,1)
w=c(9,4,5)
v=c(2,2,0)
x+y*z-w^v
```
voire aux vecteurs dont les longeurs de l'un est multiple de l'autre.
```{r,eval=T}
(1:3)+2
(1:3)+(1:6)
(1:6)+(1:3)
(1:6)^2
```

- Exercice 
Calculer la somme des racines carrées des 1000 premiers mutiples de 3


---

<details>
  <summary>Solution</summary>
  
```{r,eval=T}
((1:1000)*3)|>sqrt()|>sum()
```

</details>

---


- Exercice 
Créer le vecteur des dix premiers termes de la suite arithmétique de raison 2 et de valeur initiale 3 

---

<details>
  <summary>Solution</summary>
  
```{r,eval=T}
seq(3,by=2,length.out=10)
3+2*(0:9)
```

</details>

---



### Fonctions de vecteurs numériques.

On génère un vecteur aléatoire de longueur 1000.
```{r,eval=T}
x=rnorm(1000,0,1)
plot(x)
```

- signe et valeur absolue

```{r,eval=T}
sign(-1:1)
abs(-1:1)
```



On calcule les quantiles, l'écart type, la variance empirique, la moyenne de `x`


```{r,eval=T}
quantile(x,probs = c(0,.25,.5,.75,1))
sd(x)
var(x)
mean(x)
range(x)
```

```{r,eval=T}
plot(cumsum(x),type='l')
plot(cumprod(sort(x)),type='l')
```


### Tri, rang, ordre

Le tri retourne un vecteur de même taille, mais ordonné.
Le rang indique combien d'éléments sont plus petits ou égal
L'ordre indique la position du premier, du second ...

```{r,eval=F}
sort(x,descending=T)
rang(x)
order(x)
```

