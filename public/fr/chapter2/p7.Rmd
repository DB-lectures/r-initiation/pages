---
title: "Les booléens"
weight: 6
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---

### Création

Le résultat de la commande suivante

```{r,eval=F}
1<2;
```

peut être stocké dans un objet

```{r,eval=T}
x<-1<2;
x
```
Cet objet est un booléen

```{r,eval=T}
class(1<2)
```

D'autres opérateurs permettent d'obtenir des booléens
```{r,eval=F}
1==2
1!=2
1>=2
1<=2
1>2
1<2
```


Il peut prendre deux valeurs `TRUE` ou `FALSE`.
Ces deux valeurs peuvent être utilisées avec des opérateurs binaires spécifiques aux booleens


```{r,eval=F}
(1==2)|TRUE
(1!=2)&FALSE
any(1>=2,TRUE)
all(1<=2,2<=3,3<=4,4<4,TRUE)
!all(1<=2,2<=3,3<=4,4<4,TRUE)
!any(1<=2,2<=3,3<=4,4<4,TRUE)
```

`TRUE` et `FALSE` peuvent être remplacés par 
`T` et `F`

```{r,eval=F}
T&F
T|F
```
