---
title: "R studio"
weight: 2
output: 
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>


## R vs Rstudio 
Rstudio est un EDI (environnement de développement intégré).
Rstudio est déja installé sur les postes. 

En plus de R, Rstudio permet d'avoir une interface avec plus de fonctionnalités.


## Rstudio est  libre et gratuit

Toutefois Rstudio est gratuit.
Certaines versions sont payantes (version serveur) 

## Les plus de Rstudio

Rstudio n'est pas destiné à être presse bouton.

- les outils pour visualiser la documentation
- avoir sur le même plan console et éditeur
(Non exhaustif)
