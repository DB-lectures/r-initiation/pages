---
title: "R"
weight: 1
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---
  <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
  <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  
## Le logiciel R

### Un logiciel libre

R est gratuit!

### Une interface minimaliste

- capture d'écran

### Versions

- Le langage évolue, chaque version contient des innovations 
Par exemple, depuis la version 4.1, 

```{r, eval=F}
(1:3)|>
  log()|>
  sum()
```

peut être utilisé pour remplacer

```{r, eval=F}
sum(log(1:3)))
```
