---
title: "Variable discrète"
weight: 2
output:
blogdown::html_page:
toc: false
fig_width: 6
dev: "svg"
---

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

## La fonction gf_bar

On va travailler sur la table diamonds, incluse dans le package ggplot2.
La variable cut est de type facteur, l’idéal pour une variable discrète.

```{r  echo=T,include=F}
library(dplyr)
library(ggformula)
```

```{r  echo=T,message=F,warning=F}
head(diamonds)
diamonds |> count(cut)
diamonds |> gf_bar(~cut)
```


