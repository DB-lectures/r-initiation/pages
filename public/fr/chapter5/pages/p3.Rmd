---
title: "Variable continue"
weight: 3
output:
blogdown::html_page:
toc: false
fig_width: 6
dev: "svg"
---

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

## La fonction gf_histogram

On s’intéresse à la variable price de la table diamonds, variable quantitative continue donnant le prix en dollars d’un diamant.   
La fonction gf_histogram calcule automatiquement les bornes des tranches.

```{r  echo=T,include=F}
library(dplyr)
library(ggformula)
```

```{r  echo=T,message=F,warning=F}
diamonds |>  mutate(x=cut(price,seq(0,20000,by=1000))) |>  count(x)
diamonds |>  gf_histogram(~price)
```


