---
title: "Série chronologique"
weight: 8
output:
blogdown::html_page:
toc: false
fig_width: 6
dev: "svg"
---

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

## La fonction gf_line

A partir de la table economics, on veut illustrer l’évolution du nombre de chômeurs (variable unemploy) au cours du temps.  
La fonction gf_line relie les points par ordre d’apparition dans la table. Elle n’est pas spécifique aux séries chronologiques.

```{r  echo=T,include=F}
library(dplyr)
library(ggformula)
```

```{r  echo=T,message=F,warning=F}
head(economics, n=3)
economics |> gf_line( unemploy ~ date )
```


