---
title: "Les fonctions des autres"
weight: 5.1
---

- Les fonctions de base de R permettent de faire beaucoup de choses, mais certains utilisateurs proposent parfois des fonctions très utiles.

- Ces fonctions sont généralement partagées via des paquets.

- Un paquet ne vient pas nécessairement pré installé avec R, il faut l'installer.

- Installer un paquet revient à copier des scripts et d'autres fichiers sur son disque dur, sans avoir à se soucier de ce qui a été copié ni où. Installer un paquet se fait entre autres via la fonction `install.packages`

- Une fois un paquet installé (par example stringr), les fonctions qu'il proposent peuvent être appelées de deux manières.
    - en appelant la commande précédée du nom du package et de ::
    - en chargeant toutes les fonctions du paquet via la commande `library`, 
    
```{r,eval=T}
exists("invlogit")
LaplacesDemon::invlogit
library("LaplacesDemon")
exists("invlogit")
```

### Pour ce cours.
- Les paquets nécessaires pour ce cours sont déja installés dans un dossier d'Aus.
Pour ne pas avoir à les réinstaller, il suffit d'indiquer le chemin à R:

```{r,eval=F}
.LibPaths("V:/PALETTES/IGoR/R-4.1.0/library")
```


### Les paquets.

- Plus d'explications sur les paquets seront données dans un chapitre ultérieur.
