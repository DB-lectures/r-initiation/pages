---
title: "Création"
weight: 1
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---
  <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
  <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  
### Création d'objets.

Pour créer un objet, il faut lui assigner:
- un nom,
- une valeur.

Pour cela, un signe d'assignation (`<-`, `->`, ou `=`). 

- Exécuter:
```{r,eval=F}
x<-0
1->y
z.ijwjijf=2
```

Un nom d'objet ne peut commencer par un nombre, ni contenir certains caractères spéciaux, dont `:`,`^`,`-`,`+`,`/`,`\`,`*`,`<`,`>`,`,`,`!`,`@`,`%`,`&`, `$`, `~`, `(`, `)`, `[`,`]`,`{`, `}`, et `;`.

Pour résumer seul le point est autorisé.

La création d'un objet ne donne pas lieu à l'affichage d'un résultat dans la console. 
Pour afficher la valeur d'un objet créé, il suffit d'éxécuter la commande qui contient son nom:

- Exécuter:

```{r,eval=F}
x<-0
x
```

- Les lettres majuscules et minuscules importent!

```{r,eval=T}
x<-1
X=2
x;X
```


Un objet peut se voir réassigner une nouvelle valeur.

- Exécuter:

```{r,eval=F}
x<-0
x
x<-1
x
```

