---
title: "Les facteurs"
weight: 15
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---


- Les variables de caractère (nom de la commune) prennent beaucoup d'espace disque.
Des codes sont créés car les entiers prennent moins de place, entre autres: il suffit d'un dictionnaire des codes pour récupérer le nom de la commune.

- Sous R, de telles variables s'appellent des facteurs. Il s'agit d'un objet composé d'un vecteur de valeurs entières et d'un dictionnaire, qui relie chaque valeur entière à un texte.

### création

On utilise la fonction `as.factor` ou `factor`

```{r,eval=T}
set.seed(1)
x<-sample(letters[1:12],1000,T)
y<-x|>as.factor()
z<-x|>factor(levels=letters[1:12],labels=month.name)
```

Pour accéder au dictionnaire, il suffit d'utiliser `levels`

```{r,eval=T}
levels(z)
```


```{r,eval=T}
set.seed(1)
sexe<-sample(1:2,1000,T)
sexe<-sexe|>factor(levels=1:2,labels=c("Homme","Femme"))
levels(sexe)
sex<-dplyr::recode_factor(sexe,Homme="Male",Femme="Female")
levels(sex)
```




### Fonction retournant des facteurs


```{r,eval=T}
age=runif(1000,100);
cut(age,breaks=10*(0:10))|>levels()
```

