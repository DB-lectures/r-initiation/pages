---
title: "Les vecteurs"
weight: 10
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---

### Vecteurs d'objets d'une même classe
```{r,eval=T}
x<-c(1,2,3)
y<-c('a','b','c')
z<-c(T,F)
x;y;z;
class(x)
class(y)
class(z)
length(x)
```

La fonction `c` pour concaténation est très importante.

```{r,eval=T}
x<-c(1,2,3)
c(1,3,x)
```

