---
title: "Opérateurs arithmétiques"
weight: 4
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---
  
  
  <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
  <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  
  
Les opérateurs binaires classiques peuvent être utilisés.

```{r,eval=F}
-1+1;
1.5*2;
-1.2/4;
x<-3^2;
x;
x-1
```

Les parenthèses aussi:
```{r,eval=F}
((-2+1)*4)/2+(1/4);
```