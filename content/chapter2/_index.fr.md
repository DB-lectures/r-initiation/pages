---
title: Les objets
weight: 10
pre: "<b>2. </b>"
chapter: true
---

### Deuxième partie

# Les objets

- R est un langage de programmation orientée objet "léger".
- R crée des objets et les garde en mémoire le temps de la session.
- Les objets sont regroupés en classes (entier, réel, chaîne de caractères, vecteur, matrice, tableau,...)
- Avant de rentrer dans le vif du sujet (charger les données, manipuler des tables, calculer, créer des tableaux, des graphiques, et des rapports), il convient de faire une liste de certaines classes d'objets courants.



