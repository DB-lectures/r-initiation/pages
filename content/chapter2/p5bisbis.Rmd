---
title: "Les procédures"
weight: 5.1
---

### Distinction entre fonctions et procédures

Une distinction est parfois faite entre une fonction, qui retourne un objet qui peut être stocké dans l'environnement global et une procédure.

Une procédure ne retourne pas forcément un objet, mais par exemple, crée un fichier, installe un paquet, supprime un objet de l'environnement...

Dans R la distinction entre procédure et fonction n'est pas faite.


### Exemple

Pour afficher l'aide sur un objet, il suffit d'utiliser la fonction `help`:

```{r,eval=F}
help(sum)
```

S'affiche ensuite l'aide dans l'onglet help.