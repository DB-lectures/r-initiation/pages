---
title: "Les chaînes de caractère"
weight: 8  
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---

### Création

```{r,eval=F}
x='Bonjour'
y="Au revoir"
```

L'échappement des caractères spéciaux

```{r,eval=T}
x='Gif\\Yvettes'
x
cat(x)
```

### Opérations

- Concaténation

```{r,eval=F}
paste('Nom','prénom')
paste('Nom','prénom',sep='|')
paste0('Nom',' ','prénom')
```

- Ordre lexicographique

```{r,eval=F}
'a'<'b'
'France'>'Alaska'
max('Pieusse','Paris','Lyon','Marseille')
```

- Extraction

```{r,eval=T}
substr('78117',start = 1,stop = 2)
substr('78117',1,2)
nchar('78117')
'78117'|>(function(x){substr(x,nchar(x)-2,nchar(x))})()
```
- Recherche, remplacement

```{r,eval=T}
gsub(pattern = 'xls',replacement = 'rda',x='fichier.xls')
grepl('to','atototata')
```


