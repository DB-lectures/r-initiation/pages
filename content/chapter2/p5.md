---
title: "Les fonctions"
weight: 5
---

  
  
Les fonctions sont des objets.
  
### Création

- On écrit function, puis on spécifie entre parenthèse le nom des arguments, entre accolades une commande R.

```{r,eval=F}
f<-function(x){x+1}
g<-function(y,z,a){y+z-a}
h<-function(y,z,a){a(y+z)}
composition<-function(h,g){function(x){h(g(x))}}

```

### Appel
Pour appeler une fonction, on appelle son
```{r,eval=F}
f(1)
x=g(2,3,4);x
h(1,2,f)
f(1+2)
composition(log,exp)(1)
```

### Commandes complexes, variables locales.

- Une suite d'opérations peuvent être définies dans une même fonction.
- des objets peuvent être créés, mais il n'auront pas d'existence dans l'environnement global. 
- A l'exécution de la fonction, un environnement spécial sera créé
- Seul le résultat de la fonction (résultat de la dernière commande) sera affiché dans la log et assigné à l'objet `.Last.value`.

```{r,eval=F}
f<-function(x){
y<-x^2
y-1
}
f(1)
y
```


### Fonction numériques prédéfinies
Les fonctions suivantes sont prédéfinies
`log`, `exp`, `cos`, `sin`, `tan`, `atan`, `floor`, `ceiling`, `round`
appliquées à un nombre, elles retournent un nombre.

D'autres fonctions numériques prédéfinies s'appliquent à un nombre arbitraire de nombres

```{r,eval=F}
min(1,2)
min(1,2,3,0)
```
Ces fonctions sont, entre autres, `min`, `max`, `sum`, `prod`.

Exercice :
- Calculer la partie entière du logarithme de log(2)
- Créer une fonction qui calcule le quotient de la division euclidienne de deux nombres.


---

<details>
  <summary>Solution</summary>
  
```{r,eval=F}
x<-function(a,b){floor(a/b)}
y<-function(a,b){a%/%b}
x(13,2)
y(13,2)
```

</details>

---


### Dans l'onglet environnement

- Les fonctions sont visibles dans l'onglet Environnement. 
- Pour voir leur valeur, on peut cliquer sur le symbole à leur droite.
- Un onglet s'ouvre dans le quadrant source, intitulé View.

### Variables locales, globales.

- Les variables globales peuvent être appelées dans une fonction.
- Dans l'exemple ci-dessous, `x` `f` et `y` sont des objets de l'environnement global.
- Lorsque la fonction est exécutée, les commandes de la fonction sont exécutées, après avoir remplacé les valeurs des arguments par celles spécifiées à l'appel de la fonction. 
- Lorsque des objets apparaissent dans les commandes de la fonction, mais ne sont pas des arguments, R va les chercher dans l'environnement global. 

```{r,eval=F}
f<-function(a,b){floor(a/b)}
x=2
y=3
g<-function(x){f(x,y)}
g(3)
y=4
g(3)
```

### Les valeurs par défaut.

Des valeurs par défaut peuvent être données à chacun des arguments

```{r,eval=F}
f<-function(a,b=2){floor(a/b)}
f(6)
f(6,3)
```

### L'ordre des arguments

Les arguments peuvent être rentrés dans un ordre ne respectant pas l'ordre de définition de la fonction en étant nommés.

```{r,eval=F}
f<-function(a,b=2){floor(a/b)}
f(6)
f(6,3)
f(b=3,a=6)
f(b=3,3)
```

