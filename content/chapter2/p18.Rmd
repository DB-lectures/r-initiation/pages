---
title: "Les dataframes"
weight: 18
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---


- Les dataframes sont des listes de vecteur de même dimension.

### Création

```{r}
df<-data.frame(x=1:12,y=month.name,z=month.abb|>as.factor())
class(df)
lapply(df,class)
```

(Presque) toutes les fonctions qui s'appliquent aux listes s'appliquent aux dataframes.


```{r}
names(df)
length(df)
c(df,df)
```


### Indexation

Les dataframes s'indexent comme des listes, mais aussi comme des matrices:

```{r}
df$x
df[["x"]]
df[1:2,c(T,F,T)]
df$w<-df$x+1
```


La suite du cours donnera plus d'indications sur la manipulation des dataframes.
