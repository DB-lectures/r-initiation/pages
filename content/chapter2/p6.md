---
title: "Les tuyaux"
weight: 6
---


- Lors d'enchainements de calculs les commandes deviennent rapidement illisibles

```{r,eval=F}
log(exp(sin(3)+min(1,2,sum(1,4))))
```
- Il faut s'y retrouver dans les parenthèses.

- Une solution pour celà est de multiplier les variables intermédiaires

```{r,eval=F}
x=sin(3)
z=sum(1,4)
a=min(1,4,z)
b=min(1,2,a)
c=x+b
log(exp(c))
```
mais on s'y perd un peu aussi.

- Souvent on prend un objet au départ (par exemple une dataframe),
puis on lui fait subir une série d'opérations: sélection de variables, fusion avec une autre table, sélection des observations, calcul d'une nouvelle variable, calcul de statistiques descriptives par groupes, fusion avec une autre table.

- Dans SAS ou dans R, chacune de ces opérations peut donner lieu à un nouvel objet.

- L'objectif du tuyau est de se passer des objets intermédiaires. L'objet de départ passe dans des tuyaux successifs et est transformé.
On retrouve l'esprit calculette.

```{r,eval=F}
3|>
  sin()
sin(3)
3|>
  sin()|>
  exp()|>
  round()

3|>
  sin()|>
  exp()|>
  round()->x
  x
```

### Fonctions de plusieurs variables

Le tuyau place l'objet en premier argument non nommé de la fonction qui suit le tuyau.

```{r,eval=F}
f<-function(p,q=1,r=1){p^q+r}
3|>
  f(1,0)
3|>
  f(p=2,0)
3|>
  f(p=0,q=2)
```

