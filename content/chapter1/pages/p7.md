---
title: L'historique
weight: 7
---



<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

- Le quadrant en haut à droite contient par défaut 3 onglets.
- A ce stade du cours, seul l'onglet History nous intéresse
- Il contient l'historique des commandes passées.

- Celui ci peut être supprimé en intégralité, avec le bouton
![](images/p7.1.png)
- Une commande peut être sélectionnée et recopiée soit dans la console, soit dans l'éditeur (to source) à l'endroit du curseur.
- Un menu de recherche des commandes passées est disponible.

### Exercice
- Retrouver la commande `(1+sqrt(5))/2` dans l'historique et la copier dans la console.
