---
title: "Organisation des fichiers"
weight: 3
output:
  blogdown::html_page:
    toc: false
    fig_width: 6
    dev: "svg"
---


    Racine
    ├── R                              
    │   ├── script1.R
    │   ├── script2.R
    ├── data
    │   ├── output1.rda
    │   ├── input1.rda
    ├── man
    │   ├── function1.rd
    │   ├── function2.rd
    ├── inst
    │   ├── extdata
    │   │   ├── fichier1.xlsx
    │   │   ├── fichier2.sas7bdat
    ├── demo                            
    │   ├── main.R                      
    │   ├── cree_tableaux pour publi.R  
    ├── vignettes                       
    │   ├── presentation 1.Rmd          
    ├── DESCRIPTION                     
    └── NAMESPACE                       
    
    
Exercice:

Créer un paquet et l'installer.



---

<details>
  <summary>Solution</summary>
  

File > New Project > 

![Choisir New directory](images/f1.png)

![Choisir R package](images/f2.png)
![Choisir un nom](images/f3.png)

```{r,eval=F}
devtools::install("")
```




</details>

---

