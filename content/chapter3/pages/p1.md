---
title: "Les principes des bonnes pratiques"
weight: 1
---


### Principes

- Efficacité
  - ne pas refaire ce qui a été fait
  - homogénéiser les pratiques
- Reproductibilité

### Avantages pour l'agent

- Tout bénéf.
  - nouveaux outils
  - règles déja établies (hors Insee)
  - coût d'entrée faible