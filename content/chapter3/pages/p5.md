---
title: "Installer depuis gitlab"
weight: 5
---

Il suffit de suivre la documentation de 

- [https://gitlab.insee.fr/d0mwta/cgi](https://gitlab.insee.fr/d0mwta/cgi)

pour avoir un exemple.

A titre expérimental, vous pouvez pousser votre propre paquet sur votre espace et essayer de l'installer depuis cet espace (et non pas en local)